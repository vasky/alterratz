<?php
    include ('connect.php');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TZ</title>
    <link rel="icon" href="http://static1.squarespace.com/static/59c055781f318d7ed8c8ff9b/t/59c0e4f937c58104c0699236/1505813753629/TZ+logo.jpg?format=1500w">

    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        body, .app{
            width: 100%;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            background: #eceff4;
            position: relative;
        }
        .app__body{
            /*max-width: 320px;*/
            overflow: hidden;
        }
        .app-block:last-child{
            margin-bottom: 0;
        }
        .app-block{
            background: white;
            padding: 25px;
            text-align: left;
            box-shadow: 0px 3px 7px 0px rgba(0, 0, 0, 0.05);
            border-radius: 4px;
            margin-bottom: 20px;
        }
        .block-title:after{
            content: 'asd';
            width: 100vw;
            height: 1px;
            left: -100px;
            bottom: -20px;
            background: #eeeeee;
            position: absolute;
        }
        .block-title{
            font-size: 18px;
            position: relative;
            color: grey;
            margin-bottom: 43px;
        }
        .contact-info__input{
            width: 100%;
            height: 40px;
            padding: 13px;
            color: dimgrey;
            border-radius: 4px;
            border: #eeeeee solid 1px;
            font-size: 14px;
            margin-bottom: 11px;
        }
        .btn{
            padding: 11px;
            border-radius: 4px;
            border: none;
        }
        .btn__add-contact:hover{
            background: #646faf;
            transform: scale(0.95);
        }
        .btn__add-contact{
            color: white;
            background: #4d59a1;
        }
        .button-block{
            width: 100%;
            display: flex;
            justify-content: flex-end;
        }
        .contacts-list__ul{
            list-style: none;
        }
        .contact:after{
            content: 'asd';
            width: 100vw;
            height: 1px;
            left: -100px;
            bottom: 5px;
            background: #eeeeee;
            position: absolute;
        }
        .contact:last-child:after{
            width: 0;
            bottom: 0;
        }
        .contact{
            display: flex;
            position: relative;
            margin-bottom: 5px;
        }
        .contacts__info{
            margin-right: 10px;
            text-align: left;
        }
        .contacts__info .contact__name{
            color: black;
            margin-bottom: 4px;
            font-size: 18px;
        }
        .contacts__info .contacts__phone{
            color: #616161;
            margin-bottom: 8px;
        }
        .icon__delete-contact:hover{
            transform: scale(0.9);
            transform: translate(0,2px);
        }
        .icon__delete-contact{
            cursor: pointer;
            display: inline-block;
            height: 5px;
        }
        .contacts-list{
            max-height: 400px;
            overflow-y: auto;
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <div class="app">
        <div class="app__body">
            <div class="add__contact app-block">
                <form action="">

                    <div class="add__contact-title block-title">Добавить контакт</div>
                    <div class="add__contact-info">
                        <input class="contact-info__input new-contact-name" type="text"
                                placeholder="Введите имя и фамилию"
                               required
                               minlength="10">
                        <input class="contact-info__input new-contact-phone" type="number"
                               placeholder="Введите номер"
                               required
                               min="5">
                    </div>
                    <div class="button-block">
                        <button class="btn btn__add-contact">Добавить</button>
                    </div>

                </form>
            </div>
            <div class="contacts-list app-block">
                <div class="contacts-list__title block-title">Список контактов</div>
                <ul class="contacts-list__ul">
                    <?php if($data !== ''){
                        foreach ($data as $el){
//                            var_dump($el);
                            ?>
                    <li class="contact" data-contact-id="<?php echo($el['id']) ?>">
                        <div class="contacts__info">
                            <div class="contact__name">
                                <?php echo($el['name']); ?>
                            </div>
                            <div class="contacts__phone">
                                <?php echo($el['phone']); ?>
                            </div>
                        </div>
                        <div class="icon__delete-contact" onclick="deleteContact(this)"><img src="./ActionDelete.png" alt=""></div>
                    </li>

                    <?php
                        }
                    } ?>
                </ul>
            </div>
        </div>
    </div>
    <script>

        const ulList = document.querySelector(".contacts-list__ul")
        const button = document.querySelector(".btn__add-contact")
        let iconsClose = document.querySelectorAll(".icon__delete-contact")


        button.addEventListener('click',e=>{
            const newContactName = document.querySelector(".new-contact-name")
            const newContactPhone = document.querySelector(".new-contact-phone")
            e.preventDefault()
            new Promise(res=>{
                queryInsert(newContactName.value,newContactPhone.value)
                res()
            })
            setTimeout(()=>{
                fetchFunc()
            }, 100 )
            newContactName.value = ''
            newContactPhone.value = ''

        })

         function fetchFunc(){
             fetch('getData.php', { headers: { "Content-Type": "application/json; charset=utf-8" }})
                .then(res => res.json()) // parse response as JSON (can be res.text() for plain response)
                .then(response => {
                    let newUl = ''
                    response.forEach($el =>{
                        newUl += makeContact($el['name'], $el['phone'], $el['id'])
                    })
                    ulList.innerHTML = newUl
                })
                .catch(err => {
                    console.log(err)
                });
        }


         function queryInsert(name, phone) {
             setTimeout(()=>{
                 fetch(`insertValue.php` + '?' + `name=` + `${name}` + `&phone=` + `${phone}`, { headers: { method: "GET", "Content-Type": "application/json; charset=utf-8" }})
                     .then(res => res.json()) // parse response as JSON (can be res.text() for plain response)
                     .then(response => {
                         console.log(response)
                         fetchFunc()
                     })
                     .catch(err => {
                         console.log(err)
                     });
             }, 0 )
        }
        function makeContact(name, phone, id){
            const contact = `<li class="contact" data-contact-id='${id}'>
                        <div class="contacts__info">
                            <div class="contact__name">
                                ${name}
                            </div>
                            <div class="contacts__phone">
                                ${phone}
                            </div>
                        </div>
                        <div class="icon__delete-contact" onclick="deleteContact(this)"><img src="./ActionDelete.png" alt=""></div>
                    </li>`
            return contact
        }
        function deleteContact(contact){
            console.log("click")
            const necessaryContact = contact.closest('.contact')
            delContactQuery(necessaryContact.dataset.contactId)
        }
        function delContactQuery(id){
            new Promise((res)=>{
                try{
                    fetch(`deleteContact.php?id=${id}`)
                }catch (e) {
                    console.log(erorr)
                }
                res()
            })
            setTimeout(()=>{
                fetchFunc()
            }, 100 )
        }
    </script>
</body>
</html>